/* 
 * 
 * Gets the minimum height for the content div
 * Currently used in: portfolio.php, 
 * 
 */
$(function () {
    var footerWidth = $("footer").outerHeight(true);
    var executed = false;
    if (!executed)
    {
        var h = window.innerHeight;
        var minHeight = h - footerWidth;
        $("#content").css('min-height', minHeight + "px");
        executed = true;
    }
});

