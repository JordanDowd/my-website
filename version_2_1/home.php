<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <title>Jordan Dowd</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="./css/navbar.css">
        <link rel="stylesheet" href="./css/body.css">
        <link rel="stylesheet" href="./css/scrollbar.css">
        <link rel="stylesheet" href="./css/scrollToTopButton.css">

        <link href="https://fonts.googleapis.com/css?family=Josefin+Slab" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet">

        <script>
            /*var index = 0; //0 is home color, 1 is white
             $(function () {
             $("nav.navbar-default button.navbar-toggle").click(function () {
             if (index === 0)
             {
             $(".navbar").css('background-color', 'white');
             index = 1;
             } else
             {
             $(".navbar").css('background-color', '#D9B310');
             index = 0;
             }
             });
             });*/
        </script>
        <script src="js/setHeaderHeight.js"></script>
        <script src="js/navbarDropDown.js"></script>
        <script src="js/animateImage.js"></script>
        <script src="js/scrollToTop.js"></script>
        
    </head>
    <body>
        <div class="nav-bg"></div>
        <?php include_once './inc/navbar.php'; ?>
        <div id="content">
            <header>
                <!-- main text -->
                <div class="container-fluid_0 text-center">
                    <h1><strong>HELLO</strong></h1>
                    <h1>MY NAME IS <strong>JORDAN</strong></h1>
                    <h1><strong>WELCOME</strong> TO MY LITTLE SPOT ON THE INTERNET</h1>
                    <!--<a href="#" class="btn btn-outlined btn-theme btn-lg" data-wow-delay="0.7s">Portfolio</a>-->
                </div>
            </header>
            <div class="container-fluid">
                <div class="col-lg-1"></div>
                <div class="col-lg-5"><h2>Ive have created logos for myself and two local clubs in my area, this crest was designed for the CMUFP airtiscity team</h2></div>
                <div class="col-lg-5"><img src="./img/CMUFP.jpg" class="img-responsive" id="image_1"></div>
                <div class="col-lg-1"></div>
            </div>
            <div class="container-fluid" id="parallax">
                <div class="col-lg-1"></div>
                <div class="col-lg-5"><img src="./img/CMUFP.jpg" class="img-responsive" id="image_2"></div>
                <div class="col-lg-5"><h2>Ive have created logos for myself and two local clubs in my area, this crest was designed for the CMUFP airtiscity team</h2></div>
                <div class="col-lg-1"></div>
            </div>
            <div class="container-fluid">
                <div class="col-lg-1"></div>
                <div class="col-lg-5"><h2>Ive have created logos for myself and two local clubs in my area, this crest was designed for the CMUFP airtiscity team</h2></div>
                <div class="col-lg-5"><img src="./img/CMUFP.jpg" class="img-responsive" id="image_3"></div>
                <div class="col-lg-1"></div>
            </div>
        </div>
        <?php include_once './inc/scrollToTopButton.php'; ?>
    </body>
</html>
