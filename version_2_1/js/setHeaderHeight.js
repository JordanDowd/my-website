/*
 * 
 * Checks the height of the browser and sets the header
 * Currently used in: home.php, 
 * 
 */
$(function () {
    var executed = false;
    if (!executed)
    {
        var h = window.innerHeight;
        $("header").css('height', h + "px");
        executed = true;
    }
});
