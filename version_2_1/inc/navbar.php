<!-- Nav bar -->
<nav class="navbar navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"  data-toggle="collapse" data-target="#navbar-collapse-2" id="menu-effect">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="navbar-brand"><a href="./home.php">
                    <img src="./img/logo_black.png" width="60px" height="60px">
                </a></div>
            <div class="navbar-text">Jordan Dowd<br>Studying at DKiT, Ireland</div>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-2">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="portfolio.php">Portfolio</a></li>
                <li><a href="#2">About</a></li>
                <li><a href="cv.php">CV</a></li>
                <li><a href="#4">Contact</a></li>
                <!--<li>
                    <a class="btn btn-default btn-outline btn-circle"  data-toggle="collapse" href="#nav-collapse2" aria-expanded="false" aria-controls="nav-collapse2">SIGN IN</a>
                </li>-->
            </ul>
            <div class="collapse nav navbar-nav nav-collapse" id="nav-collapse2">
                <form class="navbar-form navbar-right form-inline" role="form">
                    <div class="form-group">
                        <label class="sr-only" for="Email">Email</label>
                        <input type="email" class="form-control" id="Email" placeholder="Email" autofocus required />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="Password">Password</label>
                        <input type="password" class="form-control" id="Password" placeholder="Password" required />
                    </div>
                    <button type="submit" class="btn btn-success">SIGN IN</button>
                </form>
            </div>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav><!-- /.navbar -->

