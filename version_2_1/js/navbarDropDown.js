/* 
 * 
 * Opens and closes the menu pushing everything down
 * Currently used in: home.php, portfolio.php, cv.php
 * 
 */
$(document).ready(function () {
    var open = false;
    $("#menu-effect").click(function () {
        if (!open)
        {
            $("#content").animate({top: '270px'}, 200);
            open = true;
        } else
        {
            $("#content").animate({top: '0px'});
            open = false;
        }
    });
});

