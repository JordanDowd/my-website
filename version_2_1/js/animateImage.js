/* 
 * 
 * Animates the image with the user scrolls after 320px
 * Currently used in: home.php, 
 * 
 */
$(document).ready(function () {
    $(function () {

        $(document).on('scroll', function () {

            if ($(window).scrollTop() > 320) {
                $('#image_1').addClass('show');
            } else {
                $('#image_1').removeClass('show');
            }
        });
    });
});

